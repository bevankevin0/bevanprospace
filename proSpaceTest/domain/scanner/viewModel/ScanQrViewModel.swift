//
//  ScanQrViewModel.swift
//  proSpaceTest
//
//  Created by bevan christian on 21/12/22.
//

import RxSwift


class ScanQrViewModel {
    let machine: BehaviorSubject<MachineLocalModel?> = BehaviorSubject(value: nil)
    let error: PublishSubject<Error> = PublishSubject()
    private var scannerLocalStorageUseCase: ScannerUseCase
    
    init(
        scannerLocalStorageUseCase: ScannerUseCase = ScannerUseCaseImpl()
    ) {
        self.scannerLocalStorageUseCase = scannerLocalStorageUseCase
    }
    
    
    func getMachine(qrNumber: String) {
        self.scannerLocalStorageUseCase.getSpecificMachine(qrNumber: qrNumber) { [weak self] response in
            switch response {
            case .success(let machine):
                self?.getFileFromFileManager(machines: machine)
            case .failure(let error):
                self?.error.onNext(error)
            }
        }
    }
    
    private func getFileFromFileManager(machines: MachineLocalModel) {
        var result = machines
        machines.imageRef.forEach { imageRef in
            if imageRef != "" {
                result.imageData.append(
                    self.scannerLocalStorageUseCase.retrieveImage(forKey: imageRef) ?? UIImage()
                )
            }
        }
        self.machine.onNext(result)
    }
}
