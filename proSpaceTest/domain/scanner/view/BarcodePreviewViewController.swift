//
//  BarcodePreviewViewController.swift
//  proSpaceTest
//
//  Created by bevan christian on 20/12/22.
//

import UIKit
import RSBarcodes_Swift
import AVFoundation

class BarcodePreviewViewController: UIViewController {

    private var barcodeNumber = ""
    private let barcodeThumbnail: UIImageView = {
        let barcodeThumbnail = UIImageView()
        barcodeThumbnail.contentMode = .scaleAspectFit
        barcodeThumbnail.clipsToBounds = true
        barcodeThumbnail.translatesAutoresizingMaskIntoConstraints = false
        return barcodeThumbnail
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .black
        self.createViewConstraint()
    }
    
    convenience init(
        barcodeNumber: String
    ) {
        self.init()
        self.barcodeThumbnail.image = createBarcode(barcodeNumber: barcodeNumber)
    }
    
    
    private func createViewConstraint() {
        self.view.addSubview(barcodeThumbnail)
        
        barcodeThumbnail.leadingAnchor.constraint(
            equalTo: self.view.leadingAnchor,
            constant: 20
        ).isActive = true
        barcodeThumbnail.topAnchor.constraint(
            equalTo: self.view.safeAreaLayoutGuide.topAnchor
        ).isActive = true
        barcodeThumbnail.trailingAnchor.constraint(
            equalTo: self.view.trailingAnchor,
            constant: -20
        ).isActive = true
        barcodeThumbnail.bottomAnchor.constraint(
            equalTo: self.view.bottomAnchor
        ).isActive = true
    }
    
    
    private func createBarcode(barcodeNumber: String) -> UIImage {
        return RSUnifiedCodeGenerator.shared.generateCode(
           barcodeNumber,
           machineReadableCodeObjectType: AVMetadataObject.ObjectType.qr.rawValue
        ) ?? UIImage()
    }
}
