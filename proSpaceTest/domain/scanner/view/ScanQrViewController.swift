//
//  ScanQrViewController.swift
//  proSpaceTest
//
//  Created by bevan christian on 19/12/22.
//

import UIKit
import RxSwift
import RSBarcodes_Swift
import AVFoundation

class ScanQrViewController: RSCodeReaderViewController {
    
    let scanQrViewModel = ScanQrViewModel()
    let barcodeValue: PublishSubject<String> = PublishSubject()
    private let disposeBag = DisposeBag()
    private var alert: Alert?
    private var barcodeFound = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.focusMarkLayer.strokeColor = UIColor.blue.cgColor
        self.cornersLayer.strokeColor = UIColor.red.cgColor
        self.setOwnBinding()
        self.setViewModelBinding()
        self.setBarcodeHandler()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.barcodeFound = false
        self.navigationController?.navigationBar.prefersLargeTitles = false
    }
    
    private func setBarcodeHandler() {
        self.barcodesHandler = { barcodes in
            for barcode in barcodes {
                guard let barcodeNumber = barcode.stringValue else { return }
                self.barcodeValue.onNext(barcodeNumber)
            }
        }
    }

    private func setOwnBinding() {
        barcodeValue
            .throttle(.seconds(2), scheduler: MainScheduler.instance)
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { [weak self] barcodeNumber in
                self?.scanQrViewModel.getMachine(qrNumber: barcodeNumber)
            })
            .disposed(by: disposeBag)
    }
    
    private func setViewModelBinding() {
        scanQrViewModel
            .machine
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { [weak self] machine in
                guard let machine = machine  else { return }
                self?.goToMachineDetailPage(machine: machine)
            })
            .disposed(by: disposeBag)
        
        scanQrViewModel
            .error
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { [weak self] error in
                guard let self else { return }
                let coreDataError = error as? CoreDataError
                self.showAlert(title: coreDataError?.message ?? "Barcode Not Found")
            }).disposed(by: disposeBag)
    }
    
    private func goToMachineDetailPage(machine: MachineLocalModel) {
        if barcodeFound == false {
            barcodeFound = true
            let detailVc = DetailMachineViewController(machine: machine)
            self.navigationController?.pushViewController(detailVc, animated: true)
        }
    }
    
    private func showAlert(
        title: String,
        autoClose: Bool = true
    ) {
        self.alert?.clearAlert()
        self.alert = Alert(
            title: title,
            autoClose: autoClose
        )
        self.alert?.showAlert()
    }

}
