//
//  ScannerLocalStorageUseCaseImpl.swift
//  proSpaceTest
//
//  Created by bevan christian on 21/12/22.
//

import UIKit

class ScannerUseCaseImpl: ScannerUseCase {
    
    var coreDataService: CoreDataService
    var fileManagerService: FileManagerService
    
    init(
        coreDataService: CoreDataService = CoreDataServiceImpl.shared,
        fileManagerService: FileManagerService = FileManagerServiceImpl.shared
    ) {
        self.coreDataService = coreDataService
        self.fileManagerService = fileManagerService
    }
    
    
    func getSpecificMachine(qrNumber: String, completion: (Result<MachineLocalModel, Error>) -> Void) {
        self.coreDataService.getSpecificMachine(qrNumber: qrNumber, completion: completion)
    }
    
    func retrieveImage(
        forKey key: String
    ) -> UIImage? {
        fileManagerService.retrieveImage(forKey: key)
    }
    
}
