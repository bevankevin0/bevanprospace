//
//  ScannerLocalStorageUseCase.swift
//  proSpaceTest
//
//  Created by bevan christian on 21/12/22.
//

import UIKit

protocol ScannerUseCase {
    func getSpecificMachine(
        qrNumber: String,
        completion: (Result<MachineLocalModel, Error>) -> Void
    )
    
    func retrieveImage(
        forKey key: String
    ) -> UIImage?
}
