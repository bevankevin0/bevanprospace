//
//  BaseViewController.swift
//  proSpaceTest
//
//  Created by bevan christian on 20/12/22.
//

import UIKit

class BaseViewController: UIViewController {
    
    weak var alertDelegate: AlertDelegate? {
        didSet {
            self.alert?.delegate = alertDelegate
        }
    }
    
    private lazy var loadingView: LoadingView = {
        let loadingView = LoadingView()
        loadingView.translatesAutoresizingMaskIntoConstraints = false
        loadingView.backgroundColor = .darkGray.withAlphaComponent(0.5)
        return loadingView
    }()
    
    private var alert: Alert?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.hideKeyboardWhenTappedAround() 
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.systemBackground
        self.navigationController?.navigationBar.tintColor = UIColor.blue
        navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.black]
    }
    
    func showAlert(
        title: String,
        autoClose: Bool = true
    ) {
        self.alert?.clearAlert()
        self.alert = Alert(
            title: title,
            autoClose: autoClose
        )
        self.alert?.delegate = alertDelegate
        self.alert?.showAlert()
    }
    
    func clearAlert() {
        self.alert?.clearAlert()
    }
    
    func showLoading(
        isLoading: Bool
    ) {
        if isLoading {
            setLoadingView()
        } else {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                self.loadingView.removeFromSuperview()
            }
        }
    }
    
    private func setLoadingView() {
        loadingView.removeFromSuperview()
        self.view.addSubview(loadingView)
        loadingView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        loadingView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        loadingView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        loadingView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
    }
}
