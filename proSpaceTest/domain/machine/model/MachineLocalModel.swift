//
//  MachineLocalModel.swift
//  proSpaceTest
//
//  Created by bevan christian on 21/12/22.
//

import UIKit

struct MachineLocalModel {
    var id: String
    var name: String
    var type: String
    var qrCodeNumber: Int
    var lastMaintenance: Date
    var imageData: [UIImage]
    var imageRef: [String]
    
    
    static func mapFromMachine(machine: Machine) -> MachineLocalModel {
        return MachineLocalModel(
            id: machine.machineId ?? "",
            name: machine.name ?? "",
            type: machine.type ?? "",
            qrCodeNumber: Int(machine.qrNumber),
            lastMaintenance: machine.lastMaintenanceDate ?? Date(),
            imageData: [UIImage](),
            imageRef: machine.imageRef?.components(separatedBy: ",") ?? [String]()
        )
    }
}
