//
//  MachineListViewModel.swift
//  proSpaceTest
//
//  Created by bevan christian on 20/12/22.
//

import RxSwift


class MachineListViewModel {
    
    let machineList: BehaviorSubject<[MachineLocalModel]> = BehaviorSubject(value: [MachineLocalModel]())
    let error: PublishSubject<Error> = PublishSubject()
    let loading: PublishSubject<Bool> = PublishSubject()

    private var machineFileManagerUseCase: MachineFileManagerUseCase
    private var machineLocalStorageUseCase: MachineLocalStorageUseCase
    
    init(
        machineFileManagerUseCase: MachineFileManagerUseCase = MachineFileManagerUseCaseImpl(),
        machineLocalStorageUseCase: MachineLocalStorageUseCase = MachineLocalStorageUseCaseImpl()
    ) {
        self.machineFileManagerUseCase = machineFileManagerUseCase
        self.machineLocalStorageUseCase = machineLocalStorageUseCase
    }
    
    func getMachine() {
        self.loading.onNext(true)
        self.machineLocalStorageUseCase.getMachine { [weak self] response in
            switch response {
            case .success(let machine):
                self?.getFileFromFileManager(machines: machine)
            case .failure(let error):
                self?.error.onNext(error)
            }
        }
    }
    
    private func getFileFromFileManager(machines: [MachineLocalModel]) {
        var result = [MachineLocalModel]()
        for index in 0..<machines.count {
            var machine = machines[index]
            machine.imageRef.forEach { imageRef in
                if imageRef != "" {
                    machine.imageData.append(
                        self.machineFileManagerUseCase.retrieveImage(forKey: imageRef) ?? UIImage()
                    )
                }
            }
            result.append(machine)
        }
        self.loading.onNext(false)
        self.machineList.onNext(result.sorted{$0.name.lowercased() < $1.name.lowercased()})
    }
    
    func sortByName() {
        self.loading.onNext(true)
        do {
            let machineList = try machineList.value()
            let sortedMachine = machineList.sorted { $0.name.lowercased() < $1.name.lowercased() }
            self.machineList.onNext(sortedMachine)
        } catch {}
        self.loading.onNext(false)
    }
    
    func sortByType() {
        self.loading.onNext(true)
        do {
            let machineList = try machineList.value()
            let sortedMachine = machineList.sorted {$0.type.lowercased() < $1.type.lowercased() }
            self.machineList.onNext(sortedMachine)
        } catch {}
        self.loading.onNext(false)
    }
    
    func deleteMachine(machine: MachineLocalModel) {
        self.loading.onNext(true)
        self.machineLocalStorageUseCase.deleteMachine(machineLocal: machine) { [weak self] response in
            switch response {
            case .success:
                do {
                    guard let machineList = try self?.machineList.value() else { return }
                    let sortedMachine = machineList.filter { data in
                        data.id != machine.id
                    }
                    self?.deleteImage(machine: machine)
                    self?.machineList.onNext(sortedMachine)
                } catch {}
            case .failure(let error):
                self?.error.onNext(error)
            }
            self?.loading.onNext(false)
        }
    }
    
    func deleteImage(
        machine: MachineLocalModel
    ) {
        machine.imageRef.forEach { imageRef in
            self.machineFileManagerUseCase.deleteImage(key: imageRef) { [weak self] response in
                switch response {
                case .success:
                    break
                case .failure(let error):
                    self?.error.onNext(error)
                }
            }
        }
    }
}
