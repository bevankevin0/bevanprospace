//
//  AddMachineViewModel.swift
//  proSpaceTest
//
//  Created by bevan christian on 19/12/22.
//

import UIKit
import RxSwift

class AddMachineViewModel {
    
    let success: PublishSubject<Bool> = PublishSubject()
    let error: PublishSubject<Error> = PublishSubject()
    let loading: PublishSubject<Bool> = PublishSubject()

    private var machineFileManagerUseCase: MachineFileManagerUseCase
    private var machineLocalStorageUseCase: MachineLocalStorageUseCase
    
    init(
        machineFileManagerUseCase: MachineFileManagerUseCase = MachineFileManagerUseCaseImpl(),
        machineLocalStorageUseCase: MachineLocalStorageUseCase = MachineLocalStorageUseCaseImpl()
    ) {
        self.machineFileManagerUseCase = machineFileManagerUseCase
        self.machineLocalStorageUseCase = machineLocalStorageUseCase
    }
    
    
    func saveMachine(machine: MachineLocalModel) {
        self.loading.onNext(true)
        self.machineLocalStorageUseCase.saveMachine(machine: machine) { [weak self] response in
            switch response {
            case .success:
                self?.saveToFileManager(machine: machine)
            case .failure(let error):
                self?.error.onNext(error)
            }
            self?.loading.onNext(false)
        }
    }
    
    
    private func saveToFileManager(machine: MachineLocalModel) {
        for index in 0..<machine.imageData.count {
            self.machineFileManagerUseCase.store(
                image: machine.imageData[index],
                forKey: machine.imageRef[index]) { [weak self] response in
                    switch response {
                    case .success(let success):
                        self?.success.onNext(success)
                    case .failure(let error):
                        self?.error.onNext(error)
                    }
                }
        }
    }
}
