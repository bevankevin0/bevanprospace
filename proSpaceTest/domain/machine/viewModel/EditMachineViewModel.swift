//
//  EditMachineViewModel.swift
//  proSpaceTest
//
//  Created by bevan christian on 20/12/22.
//

import RxSwift

class EditMachineViewModel {
    
    let successEdit: PublishSubject<Bool> = PublishSubject()
    let successDelete: PublishSubject<Int> = PublishSubject()
    let fileNotFound: PublishSubject<String> = PublishSubject()
    let error: PublishSubject<Error> = PublishSubject()
    let loading: PublishSubject<Bool> = PublishSubject()

    private var machineFileManagerUseCase: MachineFileManagerUseCase
    private var machineLocalStorageUseCase: MachineLocalStorageUseCase
    
    init(machineFileManagerUseCase: MachineFileManagerUseCase = MachineFileManagerUseCaseImpl(),
         machineLocalStorageUseCase: MachineLocalStorageUseCase = MachineLocalStorageUseCaseImpl()
    ) {
        self.machineFileManagerUseCase = machineFileManagerUseCase
        self.machineLocalStorageUseCase = machineLocalStorageUseCase
    }
    
    
    func editMachine(
        machine: MachineLocalModel
    ) {
        self.machineLocalStorageUseCase.editMachine(machine: machine) { [weak self] response in
            switch response {
            case .success:
                saveToFileManager(machine: machine)
            case .failure(let error):
                self?.error.onNext(error)
            }
        }
    }
    
    private func saveToFileManager(machine: MachineLocalModel) {
        for index in 0..<machine.imageData.count {
            self.machineFileManagerUseCase.store(
                image: machine.imageData[index],
                forKey: machine.imageRef[index]
            ) { [weak self] response in
                    switch response {
                    case .success(let success):
                        self?.successEdit.onNext(success)
                    case .failure(let error):
                        self?.error.onNext(error)
                    }
                }
        }
    }
    
    func deleteImage(
        imageRef: String,
        index: Int
    ) {
        self.machineFileManagerUseCase.deleteImage(key: imageRef) { [weak self] response in
            switch response {
            case .success:
                self?.successDelete.onNext(index)
            case .failure(let error):
                guard let error = error as? FileManagerError else { return }
                if error == .fileNotExist {
                    self?.fileNotFound.onNext(imageRef)
                } else {
                    self?.error.onNext(error)
                }
            }
        }
       
    }
}
