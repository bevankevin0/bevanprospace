//
//  MachineUseCase.swift
//  proSpaceTest
//
//  Created by bevan christian on 21/12/22.
//

import Foundation

class MachineLocalStorageUseCaseImpl: MachineLocalStorageUseCase {
    
    var coreDataService: CoreDataService

    
    init(coreDataService: CoreDataService = CoreDataServiceImpl.shared) {
        self.coreDataService = coreDataService
    }
    
    func saveMachine(machine: MachineLocalModel, completion: (Result<Bool, Error>) -> Void) {
        coreDataService.saveMachine(machine: machine, completion: completion)
    }
    
    func getMachine(completion: (Result<[MachineLocalModel], Error>) -> Void) {
        coreDataService.getMachine(completion: completion)
    }
    
    func editMachine(
        machine: MachineLocalModel,
        completion: (Result<Bool, Error>) -> Void
    ) {
        coreDataService.updateMachine(
            machineLocal: machine,
            completion: completion
        )
    }
    
    func getSpecificMachine(qrNumber: String, completion: (Result<MachineLocalModel, Error>) -> Void) {
        coreDataService.getSpecificMachine(qrNumber: qrNumber, completion: completion)
    }
    
    func updateMachine(machineLocal: MachineLocalModel, completion: (Result<Bool, Error>) -> Void) {
        coreDataService.updateMachine(machineLocal: machineLocal, completion: completion)
    }
    
    func deleteMachine(machineLocal: MachineLocalModel, completion: (Result<Bool, Error>) -> Void) {
        coreDataService.deleteMachine(machineLocal: machineLocal, completion: completion)
    }
    
}
