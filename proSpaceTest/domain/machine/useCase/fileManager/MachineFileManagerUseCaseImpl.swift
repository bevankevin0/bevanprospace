//
//  MachineFileManagerUseCase.swift
//  proSpaceTest
//
//  Created by bevan christian on 21/12/22.
//

import UIKit

class MachineFileManagerUseCaseImpl: MachineFileManagerUseCase {
    
    var fileManagerService: FileManagerService
    
    init(
        fileManagerService: FileManagerService = FileManagerServiceImpl.shared
    ) {
        self.fileManagerService = fileManagerService
    }
        
    func deleteImage(
        key: String,
        completion: (Result<Bool,Error>) -> Void
    ) {
        fileManagerService.deleteImage(forKey: key, completion: completion)
    }
    
    func retrieveImage(
        forKey key: String
    ) -> UIImage? {
        fileManagerService.retrieveImage(forKey: key)
    }
    
    func store(
        image: UIImage,
        forKey key: String,
        completion: (Result<Bool,Error>) -> Void
    ) {
        fileManagerService.store(
            image: image,
            forKey: key,
            completion: completion
        )
    }
}
