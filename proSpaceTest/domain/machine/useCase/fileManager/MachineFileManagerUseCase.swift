//
//  MachineFileManagerUseCase.swift
//  proSpaceTest
//
//  Created by bevan christian on 21/12/22.
//

import UIKit

protocol MachineFileManagerUseCase {
    func deleteImage(
        key: String,
        completion: (Result<Bool,Error>) -> Void
    )
    
    func retrieveImage(
        forKey key: String
    ) -> UIImage?
    
    func store(
        image: UIImage,
        forKey key: String,
        completion: (Result<Bool,Error>) -> Void
    )
}
