//
//  DetailMachineViewController.swift
//  proSpaceTest
//
//  Created by bevan christian on 20/12/22.
//

import UIKit
import ConvenientImagePicker
import RxSwift

protocol DetailMachineViewDelegate: AnyObject {
    func onBack()
}

class DetailMachineViewController: BaseViewController, UITextFieldDelegate {
    
    weak var delegate: DetailMachineViewDelegate?

    private let editMachineViewModel = EditMachineViewModel()
    private var selectedDate = Date()
    private let disposeBag = DisposeBag()
    private var editState = false
    private var machine: MachineLocalModel?
    private var deletedImageRef = [String]()
    private var selectedImageDict = [Int: UIImage]()
    private var selectedImage = [UIImage]() {
        didSet {
            tableView.reloadData()
        }
    }
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.backgroundColor = .white
        tableView.register(
            ImageThumbnailTableViewCell.self,
            forCellReuseIdentifier: ImageThumbnailTableViewCell.identifier
        )
        return tableView
    }()
    
    
    private lazy var machineFormView: MachineFormView = {
        let machineFormView = MachineFormView(detailPage: true)
        machineFormView.translatesAutoresizingMaskIntoConstraints = false
        machineFormView.delegate = self
        return machineFormView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        self.navigationItem.title = "Detail Machine"
        self.setViewConstraint()
        self.setNavigationButton()
        self.setUiBinding()
    }
    
    convenience init(machine: MachineLocalModel) {
        self.init()
        self.machine = machine
        self.machineFormView.setMachineFormData(machine: machine)
        self.selectedImage = machine.imageData
    }
    
    private func setViewConstraint() {
        self.view.addSubview(machineFormView)
        self.view.addSubview(tableView)
        
        machineFormView.topAnchor.constraint(
            equalTo: self.view.safeAreaLayoutGuide.topAnchor,
            constant: 20
        ).isActive = true
        machineFormView.leadingAnchor.constraint(
            equalTo: self.view.safeAreaLayoutGuide.leadingAnchor,
            constant: 20
        ).isActive = true
        machineFormView.trailingAnchor.constraint(
            equalTo: self.view.safeAreaLayoutGuide.trailingAnchor,
            constant: -20
        ).isActive = true
        machineFormView.heightAnchor.constraint(
            equalTo: self.view.heightAnchor,
            multiplier: 0.4
        ).isActive = true
        
        
        self.tableView.topAnchor.constraint(
            equalTo: self.machineFormView.bottomAnchor,
            constant: 20
        ).isActive = true
        self.tableView.leadingAnchor.constraint(
            equalTo: self.view.leadingAnchor
        ).isActive = true
        self.tableView.trailingAnchor.constraint(
            equalTo: self.view.trailingAnchor
        ).isActive = true
        self.tableView.bottomAnchor.constraint(
            equalTo: self.view.safeAreaLayoutGuide.bottomAnchor
        ).isActive = true
    }
    
    private func setUiBinding() {
        editMachineViewModel
            .successDelete
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { [weak self] index in
                self?.selectedImage.remove(at: index)
                self?.machine?.imageRef.remove(at: index)
                self?.saveChange()
            })
            .disposed(by: disposeBag)
        
        editMachineViewModel
            .successEdit
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { [weak self] succesEdit in
                self?.showAlert(title: "Updated Successfuly")
                self?.navigationController?.dismiss(animated: true)
                self?.delegate?.onBack()
            })
            .disposed(by: disposeBag)
        
        
        editMachineViewModel
            .fileNotFound
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { [weak self] imageRef in
                guard let index = self?.machine?.imageRef.firstIndex(where: { imageReference in
                    imageReference ==  imageRef
                }) else { return }
                self?.machine?.imageRef.remove(at: index)
                self?.selectedImage.remove(at: index)
            })
            .disposed(by: disposeBag)

        
        editMachineViewModel
            .loading
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { [weak self] loading in
                self?.showLoading(isLoading: loading)
            })
            .disposed(by: disposeBag)
        
        
        editMachineViewModel
            .error
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { [weak self] error in
                guard let self else { return }
                self.showAlert(title: error.localizedDescription)
            }).disposed(by: disposeBag)
    }
    
    private func setNavigationButton() {
        let addButtonItem = UIBarButtonItem(
            title: "Edit",
            style: .plain,
            target: self,
            action: #selector(editMachine)
        )
        let showBarcodeItem = UIBarButtonItem(
            title: "Barcode",
            style: .plain,
            target: self,
            action: #selector(showBarcode)
        )
        navigationItem.rightBarButtonItems = [addButtonItem, showBarcodeItem]
    }
        
    private func saveChange() {
        guard let machineId = machine?.id  else { return }
        machineFormView.setEnabledForm(isEnabled: false)
        editMachineViewModel.editMachine(machine: MachineLocalModel(
            id: machineId,
            name: machineFormView.getName(),
            type: machineFormView.getType(),
            qrCodeNumber: Int(machineFormView.getQrCode()) ?? 0,
            lastMaintenance: selectedDate,
            imageData: selectedImage,
            imageRef: machine?.imageRef ?? [String]()
        ))
    }
    
    private func changeBarButtonTitle() {
        if let rightitem = self.navigationItem.rightBarButtonItem {
            if editState {
                rightitem.title = "Edit"
            } else {
                rightitem.title = "Done"
            }
        }
        self.disabledImagePicker()
    }
    
    private func disabledImagePicker() {
        if machine?.imageRef.count ?? 0 > 10 {
            let tempImageRefArray = Array(self.machine?.imageRef.prefix(10) ?? [])
            self.machine?.imageRef = tempImageRefArray
            
            let tempSelectedImageArray = Array(self.selectedImage.prefix(10))
            self.selectedImage = tempSelectedImageArray
            
            self.machineFormView.disabledPickImage(isDisabled: true)
        } else {
            self.machineFormView.disabledPickImage(isDisabled: false)
        }
    }
    
    
    @objc private func onValueChanged(datePicker: UIDatePicker) {
        self.selectedDate = datePicker.date
    }
    
    @objc private func editMachine() {
        changeBarButtonTitle()
        if editState {
            self.saveChange()
        } else {
            machineFormView.setEnabledForm(isEnabled: true)
        }
        editState.toggle()
    }
    
    @objc private func showBarcode() {
        let barcodePreviewVc = BarcodePreviewViewController(barcodeNumber: machineFormView.getQrCode())
        self.navigationController?.pushViewController(barcodePreviewVc, animated: true)
    }
    
    @objc private func pickImageOnClick() {
        let pickerViewController = PickerViewController()
        pickerViewController.delegate = self
        pickerViewController.maxNumberOfSelectedImage = 10
        self.present(pickerViewController, animated: true, completion: nil)
    }
    
}


extension DetailMachineViewController: ConvenientImagePickerDelegate {
    func imagePickerDidCancel(_ selectedImages: [Int : UIImage]) {
        self.selectedImageDict.values.forEach { image in
            self.machine?.imageRef.append(String(image.hashValue))
            self.selectedImage.append(image)
        }
        self.disabledImagePicker()
        self.selectedImageDict.removeAll()
    }
    
    func imageDidSelect(_ imagePicker: ConvenientImagePicker.PickerViewController, index: Int, image: UIImage?) {
        guard let image = image else { return }
        self.selectedImageDict[index] = image
    }
    
    func imageDidDeselect(_ imagePicker: ConvenientImagePicker.PickerViewController, index: Int, image: UIImage?) {
        self.selectedImageDict.removeValue(forKey: index)
    }
    
    func imageSelectMax(_ imagePicker: ConvenientImagePicker.PickerViewController, wantToSelectIndex: Int, wantToSelectImage: UIImage?) {}
}

extension DetailMachineViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return selectedImage.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(
            withIdentifier: ImageThumbnailTableViewCell.identifier,
            for: indexPath
        ) as! ImageThumbnailTableViewCell
        
        cell.setImage(image: selectedImage[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let fullPreviewVc = FullPreviewThumbnailViewController(
            image: selectedImage[indexPath.row],
            currentIndex: indexPath.row
        )
        fullPreviewVc.delegate = self
        self.navigationController?.pushViewController(fullPreviewVc, animated: true)
    }
}

extension DetailMachineViewController: FullPreviewThumbnailDelegate {
    func onDelete(index: Int) {
        if let selectedImage = self.machine?.imageRef[index] {
            if selectedImage == "" {
                self.selectedImage.remove(at: index)
                self.machine?.imageRef.remove(at: index)
            } else {
                self.editMachineViewModel.deleteImage(
                    imageRef: selectedImage,
                    index: index
                )
            }
        }
  
    }
}


extension DetailMachineViewController: MachineFormViewDelegate {
    func onClickImageGalery() {
        self.pickImageOnClick()
    }
    
    func onDateChange(datePicker: UIDatePicker) {
        self.selectedDate = datePicker.date
    }
}
