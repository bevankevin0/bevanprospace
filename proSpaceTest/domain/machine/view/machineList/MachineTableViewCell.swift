//
//  MachineTableViewCell.swift
//  proSpaceTest
//
//  Created by bevan christian on 19/12/22.
//

import UIKit

class MachineTableViewCell: UITableViewCell {
    private let nameLabel: UILabel = {
        let nameLabel = UILabel()
        nameLabel.font = UIFont.systemFont(ofSize: 20, weight: .bold)
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        return nameLabel
    }()
    
    private let typeLabel: UILabel = {
        let typeLabel = UILabel()
        typeLabel.font = UIFont.systemFont(ofSize: 16, weight: .medium)
        typeLabel.translatesAutoresizingMaskIntoConstraints = false
        return typeLabel
    }()
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.initView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.initView()
    }
    
    func setCellValue(
        name: String,
        type: String
    ) {
        self.nameLabel.text = name
        self.typeLabel.text = type
    }
    
    private func initView() {
        self.contentView.addSubview(nameLabel)
        self.contentView.addSubview(typeLabel)
        
        self.setNameLabelConstraint()
        self.setTypeLabelConstraint()
    }
    
    private func setNameLabelConstraint() {
        nameLabel.leadingAnchor.constraint(
            equalTo: self.contentView.leadingAnchor,
            constant: 20
        ).isActive = true
        nameLabel.topAnchor.constraint(
            equalTo: self.contentView.topAnchor,
            constant: 10
        ).isActive = true
        nameLabel.trailingAnchor.constraint(
            equalTo: self.contentView.trailingAnchor,
            constant: -20
        ).isActive = true
    }
    
    
    private func setTypeLabelConstraint() {
        typeLabel.leadingAnchor.constraint(
            equalTo: self.contentView.leadingAnchor,
            constant: 20
        ).isActive = true
        typeLabel.topAnchor.constraint(
            equalTo: self.nameLabel.bottomAnchor,
            constant: 10
        ).isActive = true
        typeLabel.trailingAnchor.constraint(
            equalTo: self.contentView.trailingAnchor,
            constant: -20
        ).isActive = true
        
        typeLabel.bottomAnchor.constraint(
            equalTo: self.contentView.bottomAnchor,
            constant: -10
        ).isActive = true
    }
}
