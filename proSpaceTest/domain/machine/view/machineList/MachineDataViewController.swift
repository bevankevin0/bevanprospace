//
//  MachineDataViewController.swift
//  proSpaceTest
//
//  Created by bevan christian on 19/12/22.
//

import UIKit
import RxSwift

class MachineDataViewController: BaseViewController {
    
    let service = CoreDataServiceImpl.shared
    private let machineListViewModel = MachineListViewModel()
    private let disposeBag = DisposeBag()
    private var machineList = [MachineLocalModel]() {
        didSet {
            self.tableView.reloadData()
        }
    }
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.register(
            MachineTableViewCell.self,
            forCellReuseIdentifier: MachineTableViewCell.identifier
        )
        return tableView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        self.title = "Machine"
        self.setUiBinding()
        self.setViewConstraint()
        self.setNavigationButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.machineListViewModel.getMachine()
        self.navigationController?.navigationBar.prefersLargeTitles = false
    }
    
    private func setViewConstraint() {
        self.view.addSubview(tableView)
        self.tableView.topAnchor.constraint(
            equalTo: self.view.safeAreaLayoutGuide.topAnchor
        ).isActive = true
        self.tableView.leadingAnchor.constraint(
            equalTo: self.view.leadingAnchor
        ).isActive = true
        self.tableView.trailingAnchor.constraint(
            equalTo: self.view.trailingAnchor
        ).isActive = true
        self.tableView.bottomAnchor.constraint(
            equalTo: self.view.safeAreaLayoutGuide.bottomAnchor
        ).isActive = true
    }
    
    private func setUiBinding() {
        machineListViewModel
            .machineList
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { [weak self] machineList in
                self?.machineList = machineList
            })
            .disposed(by: disposeBag)
        
        machineListViewModel
            .loading
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { [weak self] loading in
                self?.showLoading(isLoading: loading)
            })
            .disposed(by: disposeBag)
        
        
        machineListViewModel
            .error
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { [weak self] error in
                guard let self else { return }
                self.showAlert(title: error.localizedDescription)
            }).disposed(by: disposeBag)
    }
    
    private func setNavigationButton() {
        let sortButtonItem = UIBarButtonItem(title: "Sort", style: .plain, target: self, action: #selector(sortMachine))
        let addButtonItem = UIBarButtonItem(title: "Add", style: .plain, target: self, action: #selector(addMachine))
        navigationItem.rightBarButtonItems = [addButtonItem, sortButtonItem]
    }
    
    
    @objc private func addMachine() {
        let addMachineVc = AddMachineViewController()
        addMachineVc.delegate = self
        let rootModal = UINavigationController(rootViewController: addMachineVc)
        self.navigationController?.present(rootModal, animated: true)
    }
    
    @objc private func sortMachine() {
        self.showActionSheet()
    }
    
    private func showActionSheet() {
        let alert = UIAlertController(title: "Sort", message: "Please Select Sort Type", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Sort by Name", style: .default, handler: { (_) in
            self.machineListViewModel.sortByName()
        }))

        alert.addAction(UIAlertAction(title: "Sort by Type", style: .default, handler: { (_) in
            self.machineListViewModel.sortByType()
        }))
        self.present(alert, animated: true)
    }

}

extension MachineDataViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return machineList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(
            withIdentifier: MachineTableViewCell.identifier,
            for: indexPath
        ) as! MachineTableViewCell
        let machine = machineList[indexPath.row]
        cell.setCellValue(name: machine.name, type: machine.type)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detailVc = DetailMachineViewController(machine: machineList[indexPath.row])
        detailVc.delegate = self
        self.navigationController?.pushViewController(detailVc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            self.machineListViewModel.deleteMachine(machine: machineList[indexPath.row])
        }
    }
    
}

extension MachineDataViewController: DetailMachineViewDelegate {
    func onBack() {
        self.machineListViewModel.getMachine()
    }
}

extension MachineDataViewController: AddMachineViewDelegate {
    func onAddNewData() {
        self.machineListViewModel.getMachine()
    }
}

extension UITableViewCell {
    static var identifier: String {
        return String(describing: self)
    }
}
