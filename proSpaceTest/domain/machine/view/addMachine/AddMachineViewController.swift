//
//  AddMachineViewController.swift
//  proSpaceTest
//
//  Created by bevan christian on 19/12/22.
//

import UIKit
import RxSwift
import ConvenientImagePicker


protocol AddMachineViewDelegate: AnyObject {
    func onAddNewData()
}

class AddMachineViewController: BaseViewController, UITextFieldDelegate {
    
    weak var delegate: AddMachineViewDelegate?
    
    private let addMachineViewModel = AddMachineViewModel()
    private var selectedDate = Date()
    private let disposeBag = DisposeBag()
    private var selectedImageDict = [Int: UIImage]()
    private var selectedImage = [UIImage]() {
        didSet {
            tableView.reloadData()
        }
    }
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.register(
            ImageThumbnailTableViewCell.self,
            forCellReuseIdentifier: ImageThumbnailTableViewCell.identifier
        )
        return tableView
    }()
    
    private lazy var machineFormView: MachineFormView = {
        let machineFormView = MachineFormView(detailPage: false)
        machineFormView.translatesAutoresizingMaskIntoConstraints = false
        machineFormView.delegate = self
        return machineFormView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        self.navigationItem.title = "Add Machine"
        self.setViewConstraint()
        self.setNavigationButton()
        self.setUiBinding()
    }
    
    private func setViewConstraint() {
        self.view.addSubview(machineFormView)
        self.view.addSubview(tableView)
        
        machineFormView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 20).isActive = true
        machineFormView.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor, constant: 20).isActive = true
        machineFormView.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: -20).isActive = true
        machineFormView.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.4).isActive = true
        
        
        self.tableView.topAnchor.constraint(
            equalTo: self.machineFormView.bottomAnchor,
            constant: 20
        ).isActive = true
        self.tableView.leadingAnchor.constraint(
            equalTo: self.view.leadingAnchor
        ).isActive = true
        self.tableView.trailingAnchor.constraint(
            equalTo: self.view.trailingAnchor
        ).isActive = true
        self.tableView.bottomAnchor.constraint(
            equalTo: self.view.safeAreaLayoutGuide.bottomAnchor
        ).isActive = true
    }
    
    private func setUiBinding() {
        addMachineViewModel
            .success
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { [weak self] loading in
                self?.showAlert(title: "Saved Successfuly")
                self?.navigationController?.dismiss(animated: true)
                self?.delegate?.onAddNewData()
            })
            .disposed(by: disposeBag)
        
        addMachineViewModel
            .loading
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { [weak self] loading in
                self?.showLoading(isLoading: loading)
            })
            .disposed(by: disposeBag)
        
        
        addMachineViewModel
            .error
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { [weak self] error in
                guard let self else { return }
                self.showAlert(title: error.localizedDescription)
            }).disposed(by: disposeBag)
    }
    
    private func setNavigationButton() {
        let addButtonItem = UIBarButtonItem(title: "Add Machine", style: .plain, target: self, action: #selector(addMachine))
        navigationItem.rightBarButtonItems = [addButtonItem]
    }
    
    
    @objc private func addMachine() {
        if machineFormView.getQrCode().isValidInput(regex: .numberOnly) && machineFormView.getQrCode() != "" && !selectedImage.isEmpty  {
            addMachineViewModel.saveMachine(machine: MachineLocalModel(
                id: UUID().uuidString,
                name: machineFormView.getName(),
                type: machineFormView.getType(),
                qrCodeNumber: Int(machineFormView.getQrCode()) ?? 0,
                lastMaintenance: selectedDate,
                imageData: selectedImage,
                imageRef: mapSelectedImage()
            ))
        } else if selectedImage.isEmpty {
            self.showAlert(title: "Please select an image")
        } else {
            self.showAlert(title: "QR number can only be filled with numbers")
        }

    }
    
    private func mapSelectedImage() -> [String] {
        var temporaryImageRef = [String]()
        selectedImage.forEach { image in
            temporaryImageRef.append(String(image.hash))
        }
        return temporaryImageRef
    }

    private func pickImageOnClick() {
        let pickerViewController = PickerViewController()
        pickerViewController.delegate = self
        pickerViewController.maxNumberOfSelectedImage = 10
        self.present(pickerViewController, animated: true, completion: nil)
    }
    
}

extension AddMachineViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return selectedImage.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(
            withIdentifier: ImageThumbnailTableViewCell.identifier,
            for: indexPath
        ) as! ImageThumbnailTableViewCell
        
        cell.setImage(image: selectedImage[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let fullPreviewVc = FullPreviewThumbnailViewController(image: selectedImage[indexPath.row], currentIndex: indexPath.row)
        fullPreviewVc.delegate = self
        self.navigationController?.pushViewController(fullPreviewVc, animated: true)
    }
    
}


extension AddMachineViewController: ConvenientImagePickerDelegate {
    func imagePickerDidCancel(_ selectedImages: [Int : UIImage]) {
        self.selectedImageDict.values.forEach { image in
            self.selectedImage.append(image)
        }
        self.selectedImageDict.removeAll()
    }
    
    func imageDidSelect(_ imagePicker: ConvenientImagePicker.PickerViewController, index: Int, image: UIImage?) {
        guard let image = image else { return }
        self.selectedImageDict[index] = image
    }
    
    func imageDidDeselect(_ imagePicker: ConvenientImagePicker.PickerViewController, index: Int, image: UIImage?) {
        self.selectedImageDict.removeValue(forKey: index)
    }
    
    func imageSelectMax(_ imagePicker: ConvenientImagePicker.PickerViewController, wantToSelectIndex: Int, wantToSelectImage: UIImage?) {}
}


extension AddMachineViewController: MachineFormViewDelegate {
    func onClickImageGalery() {
        self.pickImageOnClick()
    }
    
    func onDateChange(datePicker: UIDatePicker) {
        self.selectedDate = datePicker.date
    }
    
}
extension AddMachineViewController: FullPreviewThumbnailDelegate {
    func onDelete(index: Int) {
        self.selectedImage.remove(at: index)
    }
}
