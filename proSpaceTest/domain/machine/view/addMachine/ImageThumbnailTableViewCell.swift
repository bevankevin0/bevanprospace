//
//  ImageThumbnailTableViewCell.swift
//  proSpaceTest
//
//  Created by bevan christian on 19/12/22.
//

import UIKit

class ImageThumbnailTableViewCell: UITableViewCell {
    
    private let thumbnail: UIImageView = {
        let thumbnail = UIImageView()
        thumbnail.contentMode = .scaleAspectFit
        thumbnail.clipsToBounds = true
        thumbnail.translatesAutoresizingMaskIntoConstraints = false
        return thumbnail
    }()

    override func awakeFromNib() {
        super.awakeFromNib()
        self.createUi()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.createUi()
    }
    
    func setImage(image: UIImage) {
        self.thumbnail.image = image
    }
    
    private func createUi() {
        self.contentView.addSubview(thumbnail)
        
        thumbnail.leadingAnchor.constraint(
            equalTo: self.contentView.leadingAnchor
        ).isActive = true
        thumbnail.topAnchor.constraint(
            equalTo: self.contentView.topAnchor,
            constant: 10
        ).isActive = true
        thumbnail.trailingAnchor.constraint(
            equalTo: self.contentView.trailingAnchor
        ).isActive = true
        thumbnail.bottomAnchor.constraint(
            equalTo: self.contentView.bottomAnchor,
            constant: -10
        ).isActive = true
        thumbnail.heightAnchor.constraint(
            equalToConstant: 80
        ).isActive = true
        
        thumbnail.widthAnchor.constraint(
            equalToConstant: 80
        ).isActive = true
    }

}
