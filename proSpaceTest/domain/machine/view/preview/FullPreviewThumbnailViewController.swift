//
//  FullPreviewThumbnailViewController.swift
//  proSpaceTest
//
//  Created by bevan christian on 19/12/22.
//

import UIKit


protocol FullPreviewThumbnailDelegate: AnyObject {
    func onDelete(index: Int)
}
class FullPreviewThumbnailViewController: UIViewController {
    
    
    weak var delegate: FullPreviewThumbnailDelegate?
    private var currentIndex = 0
    private var editState = true
    private let thumbnail: UIImageView = {
        let thumbnail = UIImageView()
        thumbnail.contentMode = .scaleAspectFit
        thumbnail.clipsToBounds = true
        thumbnail.translatesAutoresizingMaskIntoConstraints = false
        return thumbnail
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .black
        self.createViewConstraint()
    }
    
    convenience init(
        image: UIImage,
        currentIndex: Int
    ) {
        self.init()
        self.thumbnail.image = image
        self.currentIndex = currentIndex
        self.editState = isEditing
        self.setNavigationButton()
    }
    
    
    private func createViewConstraint() {
        self.view.addSubview(thumbnail)
        
        thumbnail.leadingAnchor.constraint(
            equalTo: self.view.leadingAnchor
        ).isActive = true
        thumbnail.topAnchor.constraint(
            equalTo: self.view.safeAreaLayoutGuide.topAnchor
        ).isActive = true
        thumbnail.trailingAnchor.constraint(
            equalTo: self.view.trailingAnchor
        ).isActive = true
        thumbnail.bottomAnchor.constraint(
            equalTo: self.view.bottomAnchor
        ).isActive = true
    }
    
    private func setNavigationButton() {
        let deleteButtonItem = UIBarButtonItem(
            title: "Delete",
            style: .done,
            target: self,
            action: #selector(deleteImage)
        )
        navigationItem.rightBarButtonItems = [deleteButtonItem]
    }
    
    
    @objc private func deleteImage() {
        self.delegate?.onDelete(index: currentIndex)
        self.navigationController?.popViewController(animated: true)
    }
}
