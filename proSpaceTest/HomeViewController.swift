//
//  HomeViewController.swift
//  proSpaceTest
//
//  Created by bevan christian on 19/12/22.
//

import UIKit

class HomeViewController: BaseViewController {
    
    private var firstTime = false
    
    private lazy var machineDataButton: UIButton = {
        let machineDataButton = UIButton()
        machineDataButton.translatesAutoresizingMaskIntoConstraints = false
        machineDataButton.setTitle("Machine Data", for: .normal)
        machineDataButton.setInsets(
            forContentPadding: UIEdgeInsets(top: 10, left: 20, bottom: 10, right: 20),
            imageTitlePadding: 10
        )
        machineDataButton.layer.cornerRadius = 10
        machineDataButton.addTarget(self, action: #selector(machineDataOnClick), for: .touchUpInside)
        machineDataButton.backgroundColor = .red
        return machineDataButton
    }()
    
    
    private lazy var qrCodeReaderButton: UIButton = {
        let qrCodeReaderButton = UIButton()
        qrCodeReaderButton.translatesAutoresizingMaskIntoConstraints = false
        qrCodeReaderButton.setTitle("Scan Qr Code", for: .normal)
        qrCodeReaderButton.backgroundColor = .green
        qrCodeReaderButton.setInsets(
            forContentPadding: UIEdgeInsets(top: 10, left: 20, bottom: 10, right: 20),
            imageTitlePadding: 10
        )
        qrCodeReaderButton.layer.cornerRadius = 10
        qrCodeReaderButton.addTarget(self, action: #selector(qrCodeOnClick), for: .touchUpInside)
        return qrCodeReaderButton
    }()
    
    
    override func loadView() {
        super.loadView()
        if UserDefaults.standard.bool(forKey: "isNotFirstTime") == false {
            self.firstTime = true
        } else {
            self.firstTime = false
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        self.title = "Image Machine"
        self.setViewConstraint()
        self.handlingFirstTimeNavigation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    private func handlingFirstTimeNavigation() {
        if self.firstTime {
            UserDefaults.standard.setValue(true, forKey: "isNotFirstTime")
            let machineDataVc = MachineDataViewController()
            self.navigationController?.pushViewController(machineDataVc, animated: true)
        }
    }
    
    private func setViewConstraint() {
        self.view.addSubview(machineDataButton)
        self.view.addSubview(qrCodeReaderButton)
        
        machineDataButton.centerXAnchor.constraint(
            equalTo: self.view.centerXAnchor
        ).isActive = true
        machineDataButton.centerYAnchor.constraint(
            equalTo: self.view.centerYAnchor
        ).isActive = true
        
        qrCodeReaderButton.centerXAnchor.constraint(
            equalTo: self.view.centerXAnchor
        ).isActive = true
        qrCodeReaderButton.topAnchor.constraint(
            equalTo: self.machineDataButton.bottomAnchor,
            constant: 20
        ).isActive = true
        
    }
    
    @objc private func qrCodeOnClick() {
        let scanQrVc = ScanQrViewController()
        self.navigationController?.pushViewController(scanQrVc, animated: true)
    }
    
    
    @objc private func machineDataOnClick() {
        let machineDataVc = MachineDataViewController()
        self.navigationController?.pushViewController(machineDataVc, animated: true)
    }
    
}
