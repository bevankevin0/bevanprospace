//
//  CoreDataService.swift
//  proSpaceTest
//
//  Created by bevan christian on 19/12/22.
//

import Foundation
import CoreData

enum CoreDataError: Error {
    case deletionError
    case notFound
    
    var message: String {
        switch self {
        case .deletionError:
            return "Deletion error please try again"
        case .notFound:
            return "Barcode not found, please input a machine with the correct barcode."
        }
    }
}

class CoreDataServiceImpl: CoreDataService {
    static var shared = CoreDataServiceImpl()
    var container: NSPersistentContainer!
   
    
    init() {
        self.createPeristenceContainer()
    }
    
    private func createContext() -> NSManagedObjectContext {
        let taskContext = container.newBackgroundContext()
        return taskContext
    }
    
    private func createPeristenceContainer() {
        container = NSPersistentContainer(name: "MachineModel")
        container.loadPersistentStores(completionHandler: { description, error in
            if error != nil {
                fatalError("Core data not load")
            }
        })
    }
    
    func getMachine(completion: (Result<[MachineLocalModel], Error>) -> Void) {
        let context = self.createContext()
        context.performAndWait {
            let request:NSFetchRequest<Machine> = Machine.fetchRequest()
            do {
                let result = try context.fetch(request)
                let localMachine = result.map({MachineLocalModel.mapFromMachine(machine: $0)})
                completion(.success(localMachine))
            } catch let err  {
                completion(.failure(err))
            }
        }
    }
    
    func getSpecificMachine(qrNumber: String, completion: (Result<MachineLocalModel, Error>) -> Void) {
        let context = self.createContext()
        context.performAndWait {
            let request:NSFetchRequest<Machine> = Machine.fetchRequest()
            request.predicate = NSPredicate(format: "\(#keyPath(Machine.qrNumber)) == %@", qrNumber)
            do {
                let result = try context.fetch(request)
                guard let localMachine = result.compactMap({
                    MachineLocalModel.mapFromMachine(machine: $0)
                }).first else { return completion(.failure(CoreDataError.notFound))}
                completion(.success(localMachine))
            } catch let err  {
                completion(.failure(err))
            }
        }
    }
    
    func updateMachine(machineLocal: MachineLocalModel, completion: (Result<Bool, Error>) -> Void) {
        let context = self.createContext()
        context.automaticallyMergesChangesFromParent = true
        context.performAndWait {
            let request: NSFetchRequest<Machine> = Machine.fetchRequest()
            request.predicate = NSPredicate(format: "\(#keyPath(Machine.machineId)) == %@", machineLocal.id)
            if let machine = try? context.fetch(request), let updatedMachine = machine.first {
                
                updatedMachine.machineId = machineLocal.id
                updatedMachine.name = machineLocal.name
                updatedMachine.qrNumber = Int64(machineLocal.qrCodeNumber)
                updatedMachine.lastMaintenanceDate = machineLocal.lastMaintenance
                updatedMachine.type = machineLocal.type
                updatedMachine.imageRef = machineLocal.imageRef.joined(separator: ",")
                
                do {
                    try context.save()
                    completion(.success(true))
                } catch let err {
                    completion(.failure(err))
                }
            }
        }
    }
    
    
    func saveMachine(machine: MachineLocalModel, completion: (Result<Bool, Error>) -> Void) {
        let context = self.createContext()
        context.performAndWait {
            insertVariabelToModel(context: context, machineLocal: machine)
        }
        do {
            try context.save()
            completion(.success(true))
        } catch let err {
            completion(.failure(err))
        }
    }
    
    func deleteMachine(machineLocal: MachineLocalModel, completion: (Result<Bool, Error>) -> Void) {
        let context = self.createContext()
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Machine")
        fetchRequest.predicate = NSPredicate(format: "\(#keyPath(Machine.machineId)) == %@", machineLocal.id)
        let delete = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        delete.resultType = .resultTypeCount
        if let delete = try? context.execute(delete) as? NSBatchDeleteResult {
            if delete.result != nil {
                completion(.success(true))
            } else {
                completion(.failure(CoreDataError.deletionError))
            }
        }
    }
    
    private func insertVariabelToModel(
        context:NSManagedObjectContext,
        machineLocal: MachineLocalModel
    ) {
        let machine = Machine.init(context: context)
        machine.machineId = machineLocal.id
        machine.name = machineLocal.name
        machine.qrNumber = Int64(machineLocal.qrCodeNumber)
        machine.lastMaintenanceDate = machineLocal.lastMaintenance
        machine.type = machineLocal.type
        machine.imageRef = machineLocal.imageRef.joined(separator: ",")
    }
}

