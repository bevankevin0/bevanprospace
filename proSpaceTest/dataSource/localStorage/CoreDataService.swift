//
//  CoreDataService.swift
//  proSpaceTest
//
//  Created by bevan christian on 21/12/22.
//

import Foundation

protocol CoreDataService {
    func getMachine(completion: (Result<[MachineLocalModel], Error>) -> Void)
    func getSpecificMachine(qrNumber: String, completion: (Result<MachineLocalModel, Error>) -> Void)
    func updateMachine(machineLocal: MachineLocalModel, completion: (Result<Bool, Error>) -> Void)
    func saveMachine(machine: MachineLocalModel, completion: (Result<Bool, Error>) -> Void)
    func deleteMachine(machineLocal: MachineLocalModel, completion: (Result<Bool, Error>) -> Void)
}
