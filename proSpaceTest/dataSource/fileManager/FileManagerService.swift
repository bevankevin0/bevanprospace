//
//  FileManagerService.swift
//  proSpaceTest
//
//  Created by bevan christian on 21/12/22.
//

import UIKit


protocol FileManagerService {
    func store(
        image: UIImage,
        forKey key: String,
        completion: (Result<Bool,Error>) -> Void
    )
    
    func deleteImage(
        forKey key: String,
        completion: (Result<Bool,Error>) -> Void
    )
    
    func retrieveImage(
        forKey key: String
    ) -> UIImage?
}
