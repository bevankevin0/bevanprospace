//
//  FileManagerService.swift
//  proSpaceTest
//
//  Created by bevan christian on 20/12/22.
//

import UIKit

enum FileManagerError : Error{
    case fileNotExist
}

class FileManagerServiceImpl: FileManagerService {
    
    static let shared = FileManagerServiceImpl()
    
    func store(
        image: UIImage,
        forKey key: String,
        completion: (Result<Bool,Error>) -> Void
    ) {
        let pngFile = image.compress(to: 300)
        if let filePath = filePath(forKey: key) {
            do  {
                try pngFile.write(
                    to: filePath,
                    options: .atomic
                )
                completion(.success(true))
            } catch let err {
                completion(.failure(err))
            }
        }
    }
    
    func deleteImage(forKey key: String, completion: (Result<Bool,Error>) -> Void){
        if let filePath = self.filePath(forKey: key) {
            if FileManager.default.fileExists(atPath: filePath.path) {
                do {
                    try FileManager.default.removeItem(at: filePath)
                    completion(.success(true))
                } catch {
                    completion(.failure(FileManagerError.fileNotExist))
                }
            } else {
                completion(.failure(FileManagerError.fileNotExist))
            }
        }
    }
    
    func retrieveImage(forKey key: String) -> UIImage? {
        if let filePath = self.filePath(forKey: key),
           let fileData = FileManager.default.contents(atPath: filePath.path),
           let image = UIImage(data: fileData) {
            return image
        } else {
            return nil
        }
    }
    
    private func filePath(forKey key: String) -> URL? {
        let fileManager = FileManager.default
        guard let documentURL = fileManager.urls(
            for: .documentDirectory,
            in: FileManager.SearchPathDomainMask.userDomainMask
        ).first else { return nil }
        
        return documentURL.appendingPathComponent(key + ".png")
    }
}

