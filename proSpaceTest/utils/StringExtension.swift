//
//  StringExtension.swift
//  proSpaceTest
//
//  Created by bevan christian on 21/12/22.
//

import UIKit

enum FieldType {
    case numberOnly
    
    var regexValue: String {
        switch self {
        case .numberOnly:
            return "^[0-9+]*$"
        }
    }
}

extension String {
    
    func isValidInput(regex: FieldType) -> Bool {
        let checkRegex = NSPredicate(format: "SELF MATCHES %@", regex.regexValue)
        return checkRegex.evaluate(with: self)
    }
}
