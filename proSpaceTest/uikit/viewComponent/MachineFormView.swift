//
//  MachineFormView.swift
//  proSpaceTest
//
//  Created by bevan christian on 20/12/22.
//

import UIKit

protocol MachineFormViewDelegate: AnyObject {
    func onClickImageGalery()
    func onDateChange(datePicker: UIDatePicker)
}

class MachineFormView: UIView, UITextFieldDelegate {

    weak var delegate: MachineFormViewDelegate?
    
    private var isDetailPage: Bool = false
    
    private let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.distribution = .fillProportionally
        stackView.alignment = .fill
        stackView.spacing = 10
        return stackView
    }()
    
    private let idLabel: UILabel = {
        let idLabel = UILabel()
        idLabel.translatesAutoresizingMaskIntoConstraints = false
        idLabel.font = UIFont.systemFont(ofSize: 14)
        idLabel.adjustsFontSizeToFitWidth = true
        idLabel.numberOfLines = 0
        idLabel.minimumScaleFactor = 0.8
        return idLabel
    }()
    
    private let nameTextfield: UITextField = {
        let nameTextfield = UITextField()
        nameTextfield.placeholder = "Name"
        nameTextfield.translatesAutoresizingMaskIntoConstraints = false
        nameTextfield.borderStyle = .bezel
        return nameTextfield
    }()
    
    private let typeTextfield: UITextField = {
        let typeTextfield = UITextField()
        typeTextfield.placeholder = "Type"
        typeTextfield.translatesAutoresizingMaskIntoConstraints = false
        typeTextfield.borderStyle = .bezel
        return typeTextfield
    }()
    
    private let qrNumberTextfield: UITextField = {
        let qrNumberTextfield = UITextField()
        qrNumberTextfield.placeholder = "Qr Number"
        qrNumberTextfield.translatesAutoresizingMaskIntoConstraints = false
        qrNumberTextfield.borderStyle = .bezel
        qrNumberTextfield.keyboardType = .numberPad
        return qrNumberTextfield
    }()
    
    
    private let lastMaintenaceLabel: UILabel = {
        let lastMaintenaceLabel = UILabel()
        lastMaintenaceLabel.text = "Last Maintenace Date"
        lastMaintenaceLabel.translatesAutoresizingMaskIntoConstraints = false
        return lastMaintenaceLabel
    }()
    
    private lazy var lastMaintenaceDatePicker: UIDatePicker = {
        let lastMaintenaceDatePicker = UIDatePicker()
        lastMaintenaceDatePicker.preferredDatePickerStyle = .compact
        lastMaintenaceDatePicker.translatesAutoresizingMaskIntoConstraints = false
        lastMaintenaceDatePicker.addTarget(self, action: #selector(onValueChanged), for: .valueChanged)
        lastMaintenaceDatePicker.toolTip = "Date"
        return lastMaintenaceDatePicker
    }()
    
    private lazy var pickImageButton: UIButton = {
        let pickImageButton = UIButton()
        pickImageButton.translatesAutoresizingMaskIntoConstraints = false
        pickImageButton.setTitle("Machine Image", for: .normal)
        pickImageButton.backgroundColor = .red
        pickImageButton.addTarget(self, action: #selector(pickImageOnClick), for: .touchUpInside)
        return pickImageButton
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    convenience init(
        detailPage: Bool
    ) {
        self.init()
        self.isDetailPage = detailPage
        self.setViewConstraint()
        self.setEnabledForm(isEnabled: detailPage ? false : true)
    }
    
    func setMachineFormData(machine: MachineLocalModel) {
        self.nameTextfield.text = machine.name
        self.typeTextfield.text = machine.type
        self.qrNumberTextfield.text = String(machine.qrCodeNumber)
        self.lastMaintenaceDatePicker.date = machine.lastMaintenance
        if self.isDetailPage {
            self.idLabel.text = "ID MACHINE : \(machine.id)"
            self.qrNumberTextfield.isUserInteractionEnabled = false
            self.qrNumberTextfield.backgroundColor = .lightGray
        }
    }
    
    func setEnabledForm(isEnabled: Bool) {
        self.nameTextfield.isUserInteractionEnabled = isEnabled
        self.nameTextfield.backgroundColor = isEnabled ? .white : .lightGray
        
        self.typeTextfield.isUserInteractionEnabled = isEnabled
        self.typeTextfield.backgroundColor = isEnabled ? .white : .lightGray
                
        self.lastMaintenaceDatePicker.isUserInteractionEnabled = isEnabled
        
        self.pickImageButton.isUserInteractionEnabled = isEnabled
        self.pickImageButton.backgroundColor = isEnabled ? .red : .lightGray
    }
    
    func disabledPickImage(isDisabled: Bool) {
        self.pickImageButton.isUserInteractionEnabled = !isDisabled
        self.pickImageButton.backgroundColor = !isDisabled ? .red : .lightGray
    }
        
    func getName() -> String {
        return nameTextfield.text ?? ""
    }
    
    func getType() -> String {
        return typeTextfield.text ?? ""
    }
    
    func getQrCode() -> String {
        return qrNumberTextfield.text ?? ""
    }
    
    private func setViewConstraint() {
        self.addSubview(stackView)
        if isDetailPage {
            stackView.addArrangedSubview(idLabel)
        }
        stackView.addArrangedSubview(nameTextfield)
        stackView.addArrangedSubview(typeTextfield)
        stackView.addArrangedSubview(qrNumberTextfield)
        stackView.addArrangedSubview(lastMaintenaceLabel)
        stackView.addArrangedSubview(lastMaintenaceDatePicker)
        stackView.addArrangedSubview(pickImageButton)
        

        stackView.topAnchor.constraint(
            equalTo: self.topAnchor
        ).isActive = true
        stackView.leadingAnchor.constraint(
            equalTo: self.leadingAnchor
        ).isActive = true
        stackView.trailingAnchor.constraint(
            equalTo: self.trailingAnchor
        ).isActive = true
        self.stackView.bottomAnchor.constraint(
            equalTo: self.bottomAnchor
        ).isActive = true
    }
    
    @objc private func onValueChanged(datePicker: UIDatePicker) {
        self.delegate?.onDateChange(datePicker: datePicker)
    }
    
    @objc private func pickImageOnClick() {
        self.delegate?.onClickImageGalery()
    }

}

