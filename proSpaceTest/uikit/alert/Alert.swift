//
//  Alert.swift
//  proSpaceTest
//
//  Created by bevan christian on 20/12/22.
//

import UIKit

protocol AlertDelegate: AnyObject {
    func onAlertDismissed(isDismissOnTap: Bool)
}

class Alert {
    weak var delegate: AlertDelegate?
    
    private var closeTimer: Double = 0.0
    private var alertView: AlertView? = nil
    private var timer: Timer? = nil
    private var autoClose: Bool = true
    private var currentWindow: UIWindow? = {
        var currentWindow: UIWindow? = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
        currentWindow = currentWindow ?? UIApplication.shared.windows.first
        return currentWindow
    }()
    
    init(
        data: AlertModel?,
        closeTimer: Double = 2,
        autoClose: Bool = true
    ) {
        if let data = data {
            self.alertView = AlertView(data: data)
            self.alertView?.delegate = self
        }
        self.closeTimer = closeTimer
        self.autoClose = autoClose
    }
    
    convenience init(
        title: String,
        hideCloseButton: Bool = false,
        autoClose: Bool = true
    ) {
        self.init(
            data: AlertModel(
                text: title,
                hideCloseButton: hideCloseButton
            ),
            autoClose: autoClose
        )
        
    }
    
    
    func showAlert() {
        if let alertView = self.alertView, let currentWindow = currentWindow {
            setAlert(view: alertView, currentWindow: currentWindow)
        }
    }
    
    private func setAlert(view: UIView, currentWindow: UIWindow) {
        currentWindow.addSubview(view)
        view.topAnchor.constraint(
            equalTo: currentWindow.safeAreaLayoutGuide.topAnchor, constant: 30).isActive = true
        view.leadingAnchor.constraint(
            equalTo: currentWindow.leadingAnchor).isActive = true
        view.trailingAnchor.constraint(
            equalTo: currentWindow.trailingAnchor).isActive = true
        view.transform = CGAffineTransform(translationX: 0, y: -100)
        
        UIView.animate(
            withDuration: 0.4,
            delay: 0,
            options: [.curveEaseInOut],
            animations: {
                view.transform = CGAffineTransform(translationX: 0, y: 0)
            },
            completion: { done in
                if done {
                    if self.autoClose {
                        self.timer = Timer.scheduledTimer(
                            timeInterval: self.closeTimer,
                            target: self,
                            selector: #selector(self.clearAlert),
                            userInfo: nil,
                            repeats: false
                        )
                    } else {
                        let gesture = UITapGestureRecognizer(
                            target: self,
                            action: #selector(self.onTapClearAlert)
                        )
                        view.addGestureRecognizer(gesture)
                    }
                }
            }
        )
    }
    
    @objc private func onTapClearAlert() {
        self.clearAlert(isDismissOnTap: true)
    }
    
    @objc func clearAlert(isDismissOnTap: Bool = false) {
        if let alertView = self.alertView {
            dismissAlert(view: alertView, isDismissOnTap: isDismissOnTap)
        }
    }
    
    private func dismissAlert(view: UIView, isDismissOnTap: Bool = false) {
        self.delegate?.onAlertDismissed(isDismissOnTap: isDismissOnTap)
        UIView.animate(
            withDuration: 0.4,
            delay: 0,
            options: [.curveEaseInOut],
            animations: {
                view.transform = CGAffineTransform(translationX: 0, y: -100)
            },
            completion: { done in
                if done {
                    DispatchQueue.main.async {
                        view.removeFromSuperview()
                        self.timer?.invalidate()
                        self.timer = nil
                    }
                }
            }
        )
    }
}

extension Alert: AlertViewDelegate {
    func onCloseClick() {
        self.clearAlert()
    }
}

