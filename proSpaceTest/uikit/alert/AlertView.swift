//
//  AlertView.swift
//  proSpaceTest
//
//  Created by bevan christian on 20/12/22.
//


import UIKit

class AlertView: UIView {
    
    weak var delegate: AlertViewDelegate?
    
    private var closeButtonWidthConstraint: NSLayoutConstraint?
    
    private let containerView: UIView = {
        let containerView: UIView = UIView()
        containerView.translatesAutoresizingMaskIntoConstraints = false
        containerView.clipsToBounds = true
        containerView.layer.cornerRadius = 8
        containerView.backgroundColor = .lightGray
        return containerView
    }()
    
    private let alertLabel: UILabel = {
        let alertLabel = UILabel()
        alertLabel.translatesAutoresizingMaskIntoConstraints = false
        alertLabel.clipsToBounds = true
        alertLabel.numberOfLines = 0
        alertLabel.lineBreakMode = .byWordWrapping
        alertLabel.setContentHuggingPriority(.defaultHigh, for: .vertical)
        alertLabel.font = UIFont.systemFont(ofSize: 16)
        alertLabel.textAlignment = .left
        alertLabel.textColor = .white
        return alertLabel
    }()
    
    private var closeButton: UIImageView = {
        let closeButton = UIImageView()
        closeButton.image = UIImage(systemName: "xmark")
        closeButton.tintColor = .red
        closeButton.contentMode = .scaleAspectFit
        closeButton.translatesAutoresizingMaskIntoConstraints = false
        return closeButton
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initializeView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.initializeView()
    }
    
    convenience init(data: AlertModel) {
        self.init(frame: CGRect())
        self.setLabel(
            text: data.text
        )
        self.setCloseButtonWidthConstraint(
            isHide: data.hideCloseButton
        )
    }
    
    private func initializeView() {
        self.setButtonTarget()
        self.setupOwnView()
        self.setupConstraint()
    }
    
    private func setLabel(text: String) {
        self.alertLabel.text = text
        self.alertLabel.sizeToFit()
    }
    
    
    private func setCloseButtonWidthConstraint(isHide: Bool) {
        closeButtonWidthConstraint?.isActive = false
        if isHide {
            self.closeButtonWidthConstraint = closeButton.widthAnchor.constraint(equalToConstant: 0)
            self.closeButtonWidthConstraint?.isActive = true
        } else {
            self.closeButtonWidthConstraint = closeButton.widthAnchor.constraint(equalToConstant: 25)
            self.closeButtonWidthConstraint?.isActive = true
        }
    }
    
    private func setButtonTarget() {
        let closeTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(closeButtonAction))
        self.closeButton.isUserInteractionEnabled = true
        self.closeButton.addGestureRecognizer(closeTapRecognizer)
    }
    
    @objc private func closeButtonAction() {
        self.delegate?.onCloseClick()
    }
    
    private func setupOwnView() {
        self.translatesAutoresizingMaskIntoConstraints = false
        self.layer.shadowRadius = 10
        self.layer.shadowOpacity = 0.2
        self.layer.shadowOffset = CGSize(width: 0, height: 10)
    }
    
    private func setupConstraint() {
        self.addSubview(self.containerView)
        self.containerView.addSubview(self.alertLabel)
        self.containerView.addSubview(self.closeButton)
        
        self.containerView.topAnchor.constraint(
            equalTo: self.topAnchor
        ).isActive = true
        self.containerView.leadingAnchor.constraint(
            equalTo: self.leadingAnchor,
            constant: 20
        ).isActive = true
        self.containerView.trailingAnchor.constraint(
            equalTo: self.trailingAnchor,
            constant: -20
        ).isActive = true
        self.containerView.bottomAnchor.constraint(
            equalTo: self.alertLabel.bottomAnchor,
            constant: 10
        ).isActive = true
        
        
        self.alertLabel.topAnchor.constraint(
            equalTo: self.containerView.topAnchor,
            constant:10
        ).isActive = true
        self.alertLabel.leadingAnchor.constraint(
            equalTo: self.containerView.leadingAnchor,
            constant: 20
        ).isActive = true
        self.alertLabel.trailingAnchor.constraint(
            equalTo: self.closeButton.leadingAnchor,
            constant: -10
        ).isActive = true
        
        self.closeButton.centerYAnchor.constraint(
            equalTo: self.alertLabel.centerYAnchor
        ).isActive = true
        self.closeButtonWidthConstraint = closeButton.widthAnchor.constraint(equalToConstant: 25)
        self.closeButtonWidthConstraint?.isActive = true
        self.closeButton.trailingAnchor.constraint(
            equalTo: self.containerView.trailingAnchor,
            constant: -10
        ).isActive = true
        self.closeButton.bottomAnchor.constraint(
            equalTo: self.bottomAnchor
        ).isActive = true
    }
}

