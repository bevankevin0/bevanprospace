//
//  AlertModel.swift
//  proSpaceTest
//
//  Created by bevan christian on 20/12/22.
//

import Foundation


protocol AlertViewDelegate: AnyObject {
    func onCloseClick()
}

struct AlertModel {
    var text: String
    var hideCloseButton: Bool = false
}
