//
//  LoadingView.swift
//  proSpaceTest
//
//  Created by bevan christian on 20/12/22.
//

import UIKit

class LoadingView: UIView {
    
    private let loadingText: UILabel = {
        let loadingText = UILabel()
        loadingText.text = "Loading"
        loadingText.font = UIFont.systemFont(ofSize: 20)
        loadingText.textColor = .white
        loadingText.textAlignment = .center
        loadingText.numberOfLines = 0
        loadingText.translatesAutoresizingMaskIntoConstraints = false
        return loadingText
    }()
    
    private let loadingIndicator: UIActivityIndicatorView = {
        let loadingIndicator = UIActivityIndicatorView()
        loadingIndicator.color = .white
        loadingIndicator.translatesAutoresizingMaskIntoConstraints = false
        return loadingIndicator
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func didMoveToSuperview() {
        self.initializeView()
        self.loadingIndicator.startAnimating()
    }
    

    private func initializeView() {
        self.addSubview(loadingIndicator)
        self.addSubview(loadingText)

        self.heightAnchor.constraint(equalToConstant: UIScreen.main.bounds.height).isActive = true
        loadingIndicator.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        loadingIndicator.bottomAnchor.constraint(equalTo: self.loadingText.topAnchor, constant: -10).isActive = true

        loadingText.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        loadingText.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        loadingText.heightAnchor.constraint(equalToConstant: 40).isActive = true
    }

}
