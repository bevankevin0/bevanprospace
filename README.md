
# Image Machine


iOS application to help grouping images from your photo gallery and easily access them also.



## Tech Stack

**Client:** UiKit, RxSwift, MVVM, CocoaPod

**Barcode Scanner:** RSBarcodes_Swift

**Image Picker:** ConvenientImagePicker

**Local Storage:** Core Data, FileManager



## Documentation

When the application is first installed, it will automatically open to the Machine List page. On this page, the user can:

- Sort the list of machines by name or type
- View all the machines that have been saved
- Add a new machine to the list
- Swipe on a cell to delete a machine from the list

If the user clicks on a machine in the list, they will be taken to the detail page, where they can view and edit the machine's name, type, date, and image. However, the machine's barcode value cannot be edited because it is meant to remain fixed in order to maintain consistency. On this page, the user can also view the product's barcode for the barcode scan feature.


to delete an image, the user can press the thumbnail and then be directed to the preview page, then in the navigation bar there is a delete feature to delete the image

The edit page includes several validations:

- The user is limited to selecting a maximum of 10 images
- The QR number field can only contain numbers
- The image field cannot be empty

There is also a separate view for the barcode scanner, where the user can scan the barcode of a product that has been saved in the app. If the barcode is not found, an alert will appear saying "barcode not found".

### How To Compile
- go to directory 
- pod install in terminal
- open xcworkspace
- run app


## Scan Barcode Video
https://drive.google.com/file/d/1Boaoo8VLqStxWYlUDwmJE980kqem9nA4/view?usp=share_link
## How To Scan Barcode

How to scan barcodes: prepare 2 devices

- 1 device to store barcodes
- 1 device for scanning barcodes

step 1 - create the machine and input the required image and data

step 2 - when the machine has been made go to the detail page then press the barcode in navigation bar

step 3 - screenshot the barcode then send it to another device

step 4 - move to the barcode scan page then scan the barcode that has been sent to another device

step 5 - app will go to detail machine page
## Architecture
![Architecture](https://drive.google.com/uc?export=view&id=1VeOxON6MmDYruiTfBBAPE2Ilq-qmwXs9)

## Screenshots
### Homepage 
![Homepage](https://drive.google.com/uc?export=view&id=1R0ONGXS8LLT2wyUpcQfHyr85gCZgIIY4)

### Machine List 
![Machine List](https://drive.google.com/uc?export=view&id=1UUS5Cj9UYapkyfEJDkX0rz0kigw7miEf)

### Delete feature 
![Delete feature](https://drive.google.com/uc?export=view&id=1Kvzz4D9BSvUgXb3KwQIQZ9LYsXGozE5k)

### Sort 
![Sort](https://drive.google.com/uc?export=view&id=1M2bFVSqvyOz8-7D9NykUCLLe14zzDgg0)

### Detail Machine
![Detail Machine](https://drive.google.com/uc?export=view&id=1xFWYl3MMf5--oDHTQGpuXSemTaeF5v0R)


### Preview Image
![Detail Machine](https://drive.google.com/uc?export=view&id=14bf8VR4hOrq-29ADE-K8L_L7C1cPbmIl)

### Image Galery
![Image Galery](https://drive.google.com/uc?export=view&id=1IiE2x_8-1dNEeI7d7WKyssD7AfRUC6nf)

### QR Scanner
![QR Scanner](https://drive.google.com/uc?export=view&id=1RD9gsteHE3xEnG3XpSNSPYX84osqLArY)
